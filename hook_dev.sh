#!/usr/bin/env bash

cd /var/www/provencale.interact.lu/www/html

echo '--- FILES LOADING ---'
composer install --no-suggest
php bin/console assetic:dump --env=dev

echo '--- DATABASE LOADING ---'
php bin/console doctrine:schema:drop --force --env=dev
php bin/console doctrine:schema:update --force --env=dev
php bin/console doctrine:fixtures:load --no-interaction --env=dev

php bin/console cache:warmup --env=dev