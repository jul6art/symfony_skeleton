﻿$(document).ready(function(){
    initFancybox();
    initAutosize();
    initMaintenanceSwitch();
    initFormFilter();
    disable();
    scrollToFirstError();
    setTimeout(fixNotEmptyInputError(), 1000);
    fixAnalyticChartsWidth();
    garage();
});

function initFancybox() {
    $('.fancybox').fancybox();
}

function initAutosize() {
    $('.autosize').autosize();
}

function initMaintenanceSwitch() {
    var button = $('.maintenance-switch').closest('a');
    button.click(function (e) {
        e.preventDefault();
        var checkBox = $(this).find('input');
        checkBox.prop("checked", !checkBox.prop("checked"));
        url = $(this).attr('href');
        setTimeout("location.href = url;", 300);
    });
}

function initFormFilter() {
    $('[form$="filter"]').change(function () {
        $('form#' + $(this).attr('form')).find('input[type="submit"]').trigger('click');
    });
}

function disable () {
    $('a[disabled], .disabled').click(function (e) {
        e.preventDefault();
    });
}

function fixNotEmptyInputError () {
    $('input.form-text[value]').each(function() {
        if ($(this).val() !== '') {
            $(this).addClass('notempty');
        }
    });
}

function fixAnalyticChartsWidth() {
    $('a[role="tab"]').click(function () {
        setTimeout(function () {
            $(window).trigger('resize');
        }, 200);
    });
}

function scrollToFirstError() {
    $firsterror = $(".form-animate-error:first");

    if ($firsterror.length > 0) {
        $('html, body').animate({
            scrollTop: $firsterror.offset().top - 60
        }, 2000);
        $firsterror.focus();
    }
}

function garage() {
    $('.porte a').click(function (e) {
        e.preventDefault();

        var target = $(this),
			sound = document.getElementById("garageDoorSound");

        sound.volume = 0.2;
        sound.play();

        $('.porte').addClass('open').children().hide('slow');

        setTimeout(function () {
            window.location.href = target.attr('href');
        }, 1000);
    });
}

/**
 * @param node
 * @param string
 */
function prependHtml(node, string = '') {
    html = node.html();
    node.html(string + html);
}

/**
 * @param node
 * @param string
 */
function appendHtml(node, string = '') {
    html = node.html();
    node.html(html + string);
}