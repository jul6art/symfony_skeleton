$(document).ready(function(){
    ajaxTranslate();
});

function ajaxTranslate(){
    var locale = $('td.locale').text();
    locale = locale.slice(0, 2);

    $('form.trans-form').submit(function(e){
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: $(this).serialize()
        }).success(function(response) {
            if (response.httpcode === 200) {
                bootoast({
                    message: response.message,
                    type: 'success'
                });
                setTimeout(reloadPage, 1000);
            } else if (response.httpcode > 200) {
                bootoast({
                    message: 'ERROR ' + response.httpcode + ': ' + response.message,
                    type: 'danger'
                });
            }
        }).error(function() {
            bootoast({
                message: 'ERROR 605: Error processing the ajax call.',
                type: 'danger'
            });
        });
    });
}

function reloadPage(){
    window.location.href = $('#summary .status .container .break-long-words a').attr('href');
}
