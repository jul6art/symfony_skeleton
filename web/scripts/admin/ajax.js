$(document).ready(function(){
    search();
    voter();
});

function search() {
    /**
     * @param form
     * @param input
     */
    function doSearch(form, input) {
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: {'recherche': input.val()}
        }).success(function(response) {
            $('#content').html($(response).find('#content').html());
            $('html, body').animate({
                scrollTop: $('body').offset().top
            }, 2000);
        }).error(function(error) {
            // what u want
        });

        window.history.pushState("", "", '/?searchString=' + input.val());
    }

    $('form[action$="resultats"] input[value!=""]').each(function (e) {
        doSearch($(this).closest('form'), $(this));
    });

    $('form[action$="resultats"]').keyup(function (e) {
        doSearch($(this), $(event.target));
    });
}

function voter (){
    $('body').on("click", '.voter', function(e){
        e.preventDefault();

        var button = $(e.target),
            parent = button.parent('div.rating'),
            cibles = parent.data('moto'),
            gets = button.attr('href').replace("/app_dev.php", "").split("/"),
            cote = gets[2],
            element = gets[3],
            id = gets[4];

        parent.children().fadeOut(200);

        $.blockUI();

        $.ajax({
            url: $(this).attr('href'),
            type: 'POST',
            data: {
                'vote': cote,
                'element': element,
                'id': id
            }
        }).success(function(response) {
            if (response.status == 1) {
                $('.bootoast-container').remove();

                bootoast({
                    message: 'Vote enregistré',
                    type: 'success'
                });

                var rating = parseInt(response.nvCote) * 10;
                var htmlContent = '';
                for(var i = 1; i <= 5; i++) {
                    var star = '';
                    if (rating >= (i * 20)) {
                        star = 'fa-star';
                    } else if (rating >= ((i * 20) - 10)) {
                        star = 'fa-star-half';
                    }
                    htmlContent += '<span class="fa ' + star + ' fa-2x"></span>'
                }

                button.closest('.panel-body').find('.video-v1-rate').html(htmlContent);

                var roulette = $('.roulette-wrapper .roulette');

                if(roulette.data('item') ===
                    roulette.data('count')) {
                    $('.roulette-wrapper').load(roulette.data('href') + " #roulette");
                } else {
                    roulette.data('item',
                        parseInt(roulette.data('item')) + 1);

                    roulette.animate({
                        marginLeft: '-=100%'
                    }, 300);
                }
            } else {
                bootoast({
                    message: (response.message === 'Il faut se connecter pour voter!') ? response.message + ' <a href="/login">se connecter</a>'
                        : response.message,
                    type: 'warning'
                });
            }
            $.unblockUI();
        }).error(function(error) {
            bootoast({
                message: 'Action annulée par le serveur',
                type: 'danger'
            });
            $.unblockUI();
        });
    });
}