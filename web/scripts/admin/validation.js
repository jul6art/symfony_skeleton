$(document).ready(function () {
    initValidation();
});

function initValidation() {
    $.validator.setDefaults({
        errorElement: 'em',
        errorPlacement: function(error, element) {
            $(element.closest('.form-group').addClass('form-animate-error'));
            error.appendTo(element.parent('div'));
        },
        success: function(label) {
            $(label.closest('.form-group').removeClass('form-animate-error'));
        },
    });

    var form = $('form:not([novalidate])');

    $(document.body).on('submit', form, function (e) {
        if (!$(this).valid()) {
            var formErrors = $('.form-animate-error');
            if (formErrors.length) {
                setTimeout(function (e) {
                    scrollToFirstError();
                }, 700);
            }
        }
    });

    $.each(form, function (i, formElement) {
        initFormValidation($(formElement));
    });
}

function initFormValidation(form) {
    var validate = form.validate({
        ignore: ':not(:visible):not(.fake-hidden)'
    });

    // var inputPhone = form.find('.input-phone');
    // inputPhone.each(function () {
    //     $(this).rules('add', {
    //         phone: true
    //     });
    // });

    /**
     * RECAPTCHA
     */
    var inputCaptcha = form.find('.hidden-recaptcha-field');
    inputCaptcha.each(function () {
        $(this).rules('add', {
            captcha: true
        });
    });

    $.validator.addMethod('captcha', function (value, element, param) {
        if (grecaptcha.getResponse() == '') {
            return false;
        }

        return true;
    }, $.validator.messages.captcha);

    /**
     * PASSWORDS
     */
    $.validator.addMethod('checkIdenticalPassword', function (value, element, param) {
        if ($(element).prop('required') === true) {
            return $(element).val() === $(param).val();
        }

        return true;
    }, $.validator.messages.repeated_password);

    $.validator.addClassRules('password_second', {
        checkIdenticalPassword: '.password_first'
    });

    return validate;
}

function recaptchaCallback() {
    $('.hidden-recaptcha-field').valid();
};