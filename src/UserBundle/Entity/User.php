<?php

namespace UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->dateInscription = new \Datetime();
        $this->roles = [
            'ROLE_USER',
            'ROLE_VOTEUR',
        ];
        $this->softDelete = 0;
        $this->messagesCount = 0;
        $this->followersCount = 0;
        $this->membersCount = 0;
        $this->abusesCount = 0;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="messages_count", type="integer")
     */
    private $messagesCount;

    /**
     * @var int
     *
     * @ORM\Column(name="followers_count", type="integer")
     */
    protected $followersCount;

    /**
     * @var int
     *
     * @ORM\Column(name="members_count", type="integer")
     */
    private $membersCount;

    /**
     * @var int
     *
     * @ORM\Column(name="abuses_count", type="integer")
     */
    protected $abusesCount;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var bool
     *
     * @ORM\Column(name="softDelete", type="boolean")
     */
    private $softDelete;

    /**
     * @var \DateTime
     */
    private $dateInscription;

    /**
     * @return \DateTime
     */
    public function getDateInscription()
    {
        return $this->dateInscription;
    }

    /**
     * @param \DateTime $dateInscription
     *
     * @return $this
     */
    public function setDateInscription(\DateTime $dateInscription)
    {
        $this->dateInscription = $dateInscription;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSoftDelete()
    {
        return $this->softDelete;
    }

    /**
     * @param bool $softDelete
     *
     * @return $this
     */
    public function setSoftDelete($softDelete = true)
    {
        $this->softDelete = $softDelete;

        return $this;
    }

    /**
     * @return int
     */
    public function getAbusesCount()
    {
        return $this->abusesCount;
    }

    /**
     * @param int $abusesCount
     *
     * @return $this
     */
    public function setAbusesCount($abusesCount = 0)
    {
        $this->abusesCount = $abusesCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getMessagesCount()
    {
        return $this->messagesCount;
    }

    /**
     * @param int $messagesCount
     *
     * @return $this
     */
    public function setMessagesCount($messagesCount = 0)
    {
        $this->messagesCount = $messagesCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getFollowersCount()
    {
        return $this->followersCount;
    }

    /**
     * @param int $followersCount
     *
     * @return $this
     */
    public function setFollowersCount($followersCount = 0)
    {
        $this->followersCount = $followersCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getMembersCount()
    {
        return $this->membersCount;
    }

    /**
     * @param int $membersCount
     *
     * @return $this
     */
    public function setMembersCount($membersCount = 0)
    {
        $this->membersCount = $membersCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserNotificationsCount()
    {
        return $this->messagesCount + $this->followersCount;
    }

    /**
     * @return int
     */
    public function getTotalNotificationsCount()
    {
        return $this->getUserNotificationsCount() + $this->abusesCount + $this->membersCount;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     *
     * @return $this
     */
    public function setFirstname($firstname = '')
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     *
     * @return $this
     */
    public function setLastname($lastname = '')
    {
        $this->lastname = $lastname;

        return $this;
    }
}
