<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 21/08/2018
 * Time: 01:56.
 */

namespace UserBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;

/**
 * Class UserManager.
 */
class UserManager
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->userRepository = $entityManager->getRepository(User::class);
    }

    /**
     * @return UserRepository
     */
    public function getUserRepository()
    {
        return $this->userRepository;
    }

    /**
     * @param int $id
     *
     * @return null|User
     *
     * @throws NonUniqueResultException
     */
    public function find($id = 0)
    {
        return $this->userRepository->findById($id);
    }

    /**
     * @return int
     */
    public function countAll()
    {
        return $this->userRepository->countAll();
    }

    /**
     * @return User[]
     */
    public function findAll()
    {
        return $this->userRepository->findAll();
    }

    /**
     * @param null $username
     *
     * @return array
     *
     * @throws ORMException
     */
    public function findForApi($username = null)
    {
        if (is_null($username)) {
            $users = $this->findAll();
        } else {
            $users = $this->findByUsername($username);
        }

        $result = [];

        foreach ($users as $user) {
            $result[] = [
                'id' => $user->getId(),
                'text' => $user->getUsername(),
            ];
        }

        return $result;
    }

    /**
     * @param null $username
     *
     * @return array
     *
     * @throws ORMException
     */
    public function findForForm($username = null)
    {
        if (is_null($username)) {
            $users = $this->findAll();
        } else {
            $users = $this->findByUsername($username);
        }

        $result = [];

        foreach ($users as $user) {
            $result[] = [
                $user->getUsername() => $user->getId(),
            ];
        }

        return $result;
    }

    /**
     * @param string $username
     *
     * @return mixed
     *
     * @throws ORMException
     */
    public function findByUsername($username = '')
    {
        return $this->userRepository->findByUsername($username);
    }

    /**
     * @param string $role
     *
     * @return array
     */
    public function findByRole($role = '')
    {
        return $this->userRepository->findByRole($role);
    }

    /**
     * @return array
     */
    public function findAllAdmins()
    {
        return $this->findByRole('ROLE_ADMIN');
    }

    /**
     * @param User $user
     *
     * @throws OptimisticLockException
     */
    public function save(User $user)
    {
        $this->userRepository->save($user);
    }

    /**
     * @param User $user
     *
     * @throws OptimisticLockException
     */
    public function delete(User $user)
    {
        $user
            ->setSoftDelete(true)
            ->setEnabled(false);

        $this->save($user);
    }

    /**
     * @param User $user
     *
     * @throws OptimisticLockException
     */
    public function restore(User $user)
    {
        $user
            ->setSoftDelete(false)
            ->setEnabled(true);

        $this->save($user);
    }

    /**
     * @param User $user
     *
     * @throws OptimisticLockException
     */
    public function changeStatus(User $user, $status = 0)
    {
        $user
            ->setEnabled(1 == $status);

        $this->save($user);
    }
}
