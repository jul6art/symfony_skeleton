<?php

namespace UserBundle\Form;

use AdminBundle\Form\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\User;

/**
 * Class RegistrationFormType.
 */
class RegistrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('plainPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'form.error.password.mismatch',
            'first_options' => [
                'label' => 'form.user.registration.password.first.label',
                'attr' => [
                    'class' => 'password_first form-text',
                ],
            ],
            'second_options' => [
                'label' => 'form.user.registration.password.second.label',
                'attr' => [
                    'class' => 'password_second form-text',
                ],
            ],
            'translation_domain' => 'form',
        ]);

        $builder->add('captcha', CaptchaType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }
}
