<?php

namespace UserBundle\Repository;

use AdminBundle\AbstractClasses\AbstractRepository;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use UserBundle\Entity\User;

/**
 * UserRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends AbstractRepository
{
    /**
     * @param QueryBuilder $builder
     * @param array        $values
     *
     * @return QueryBuilder
     */
    public function filterByDaterange(QueryBuilder $builder, $values = [])
    {
        if (!empty($values['left_datetime'])) {
            $this->filterByDateAfter($builder, new \DateTime($values['left_datetime']));
        }

        if (!empty($values['right_datetime'])) {
            $this->filterByDateBefore($builder, new \DateTime($values['right_datetime']));
        }

        return $builder;
    }

    /**
     * @param QueryBuilder $builder
     * @param array        $values
     *
     * @return QueryBuilder
     */
    public function filterByDateBefore(QueryBuilder $builder, \DateTime $date)
    {
        $builder
            ->andWhere($builder->expr()->lte('entity.lastLogin', ':date_before'))
            ->setParameter('date_before', $date, Type::DATETIME);

        return $builder;
    }

    /**
     * @param QueryBuilder $builder
     * @param array        $values
     *
     * @return QueryBuilder
     */
    public function filterByDateAfter(QueryBuilder $builder, \DateTime $date)
    {
        $builder
            ->andWhere($builder->expr()->gte('entity.lastLogin', ':date_after'))
            ->setParameter('date_after', $date, Type::DATETIME);

        return $builder;
    }

    /**
     * @param QueryBuilder $builder
     * @param string       $role
     *
     * @return QueryBuilder
     */
    public function filterByRole(QueryBuilder $builder, $role = '')
    {
        $builder
            ->andWhere($builder->expr()->like('entity.roles', ':role'))
            ->setParameter('role', '%'.$role.'%', Type::STRING);

        return $builder;
    }

    /**
     * @param QueryBuilder $builder
     * @param string       $status
     *
     * @return QueryBuilder
     */
    public function filterByStatus(QueryBuilder $builder, $status = '')
    {
        switch ($status) {
            case 'enabled':
                $builder->andWhere($builder->expr()->eq('entity.enabled', ':enabled'))
                        ->setParameter('enabled', 1, Type::INTEGER);
                break;
            case 'locked':
                $builder->andWhere($builder->expr()->eq('entity.enabled', ':enabled'))
                        ->andWhere($builder->expr()->eq('entity.softDelete', ':softDelete'))
                        ->setParameter('enabled', 0, Type::INTEGER)
                        ->setParameter('softDelete', 0, Type::INTEGER);
                break;
            case 'deleted':
                $builder->andWhere($builder->expr()->eq('entity.enabled', ':enabled'))
                        ->andWhere($builder->expr()->eq('entity.softDelete', ':softDelete'))
                        ->setParameter('enabled', 0, Type::INTEGER)
                        ->setParameter('softDelete', 1, Type::INTEGER);
                break;
        }

        return $builder;
    }

    /**
     * @param QueryBuilder $builder
     * @param int          $id
     *
     * @return QueryBuilder
     */
    protected function filterByUsername(QueryBuilder $builder, $username = '')
    {
        $builder
            ->andWhere($builder->expr()->like('entity.username', ':username'))
            ->setParameter('username', '%'.$username.'%', Type::STRING);

        return $builder;
    }

    /**
     * @param int $id
     *
     * @return User|null
     *
     * @throws NonUniqueResultException
     */
    public function findById($id = 0)
    {
        $builder = $this->createQueryBuilder('entity');

        $this
            ->filterById($builder, $id);

        $builder
            ->setMaxResults(1);

        return $builder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $role
     *
     * @return array
     */
    public function findByRole($role = '')
    {
        $builder = $this->createQueryBuilder('entity');

        $this
            ->filterByRole($builder, $role);

        return $builder->getQuery()->getResult();
    }

    /**
     * @param string $username
     *
     * @return array
     */
    public function findByUsername($username = '')
    {
        $builder = $this->createQueryBuilder('entity');

        $this
            ->filterByUsername($builder, $username);

        return $builder->getQuery()->getResult();
    }

    /**
     * @return QueryBuilder
     */
    public function createQuery()
    {
        return $this->createQueryBuilder('entity');
    }
}
