<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 20/08/2018
 * Time: 21:27.
 */

namespace UserBundle\Doctrine\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use UserBundle\Entity\User;

/**
 * Class LoadUserData.
 */
class LoadUserData extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // User
        $user = $this->createUser('user', 'user', 'user@test.com', 'John', 'Doe');
        $this->addReference('user_user', $user);
        $manager->persist($user);

        // Admin
        $user = $this->createUser('admin', 'admin', 'admin@test.com', 'Tony', 'Admin');

        $user->addRole('ROLE_ADMIN');
        $this->addReference('user_admin', $user);
        $manager->persist($user);

        $manager->flush();
    }

    /**
     * @param string $username
     * @param string $password
     * @param string $email
     *
     * @return $this|static
     */
    private function createUser($username = '', $password = '', $email = '', $firstname = '', $lastname = '')
    {
        $user = (new User())
            ->setUsername($username)
            ->setPlainPassword($password)
            ->setEmail($email)
            ->setFirstname($firstname)
            ->setLastname($lastname)
            ->setMessagesCount(2)
            ->setFollowersCount(3)
            ->setMembersCount(4)
            ->setAbusesCount(5)
            ->setEnabled(true);

        return $user;
    }
}
