<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gkratz\MaintenanceBundle\Model\Maintenance as BaseMaintenance;

/**
 * Maintenance.
 *
 * @ORM\Table(name="maintenance")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaintenanceRepository")
 */
class Maintenance extends BaseMaintenance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var array
     *
     * @ORM\Column(name="exception_ips", type="array")
     */
    private $exceptionIps;

    public function __construct()
    {
        parent::__construct();
        $this->exceptionIps = [];
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getExceptionIps()
    {
        return $this->exceptionIps;
    }

    /**
     * @param $exceptionIp
     *
     * @return bool
     */
    public function containExceptionIp($exceptionIp)
    {
        if (in_array($exceptionIp, $this->exceptionIps)) {
            return true;
        }

        return false;
    }

    /**
     * @param $exceptionIp
     *
     * @return $this
     */
    public function addExceptionIp($exceptionIp)
    {
        if (!in_array($exceptionIp, $this->exceptionIps)) {
            $this->exceptionIps[] = $exceptionIp;
        }

        return $this;
    }

    /**
     * @param $exceptionIp
     *
     * @return $this
     */
    public function removeExceptionIp($exceptionIp)
    {
        if (false !== $key = array_search($exceptionIp, $this->exceptionIps, true)) {
            unset($this->exceptionIps[$key]);
            $this->exceptionIps = array_values($this->exceptionIps);
        }

        return $this;
    }

    /**
     * @param array $exceptionIps
     *
     * @return $this
     */
    public function setExceptionIps(array $exceptionIps)
    {
        $this->exceptionIps = $exceptionIps;

        return $this;
    }
}
