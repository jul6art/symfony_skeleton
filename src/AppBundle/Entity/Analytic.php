<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gkratz\AnalyticBundle\Model\Analytic as BaseAnalytic;

/**
 * Analytic.
 *
 * @ORM\Table(name="analytic")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnalyticRepository")
 */
class Analytic extends BaseAnalytic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Analytic constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }
}
