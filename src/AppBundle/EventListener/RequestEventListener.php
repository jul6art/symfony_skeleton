<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 22/08/2018
 * Time: 09:49.
 */

namespace AppBundle\EventListener;

use AdminBundle\AbstractClasses\AbstractEventListener;
use AppBundle\Manager\AnalyticManager;
use AppBundle\Manager\MaintenanceManager;
use AdminBundle\Service\IpService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class RequestEventListener.
 */
class RequestEventListener extends AbstractEventListener
{
    /**
     * @var MaintenanceManager
     */
    private $maintenanceManager;

    /**
     * @var AnalyticManager
     */
    private $analyticManager;

    /**
     * @var IpService
     */
    private $ipService;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * UserEventListener constructor.
     *
     * @param SessionInterface $session
     */
    public function __construct(
        SessionInterface $session,
        MaintenanceManager $maintenanceManager,
        AnalyticManager $analyticManager,
        IpService $ipService,
        RouterInterface $router,
        RequestStack $requestStack
    ) {
        parent::__construct($session);
        $this->maintenanceManager = $maintenanceManager;
        $this->analyticManager = $analyticManager;
        $this->ipService = $ipService;
        $this->router = $router;
        $this->requestStack = $requestStack;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
            KernelEvents::FINISH_REQUEST => 'onKernelFinishRequest',
        ];
    }

    /**
     * @param GetResponseEvent $event
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();

        if ($request->isXmlHttpRequest()) {
            return;
        }

        $this->setLocale($request);
        $this->setRouterContext($request);

        $requestUri = $event->getRequest()->getRequestUri();

        if (!preg_match('/admin/', $requestUri)
            && !preg_match('/maintenance/', $requestUri)) {
            $maintenance = $this->maintenanceManager->find();
            if ($maintenance->getMaintenance()
                && !$maintenance->containExceptionIp($this->ipService->getIp())) {
                $event->setResponse(new RedirectResponse($this->router->generate(
                    'app_maintenance'
                )));
            } else {
                $this->analyticManager->create($request);
            }
        }
    }

    /**
     * @param FinishRequestEvent $event
     */
    public function onKernelFinishRequest(FinishRequestEvent $event)
    {
        if (null !== $parentRequest = $this->requestStack->getParentRequest()) {
            $this->setRouterContext($parentRequest);
        }
    }

    /**
     * @param Request $request
     */
    private function setLocale(Request $request)
    {
        if ($locale = $request->get('_locale')) {
            $request->setLocale($locale);
            $request->getSession()->set('_locale', $locale);
        } else {
            $request->setLocale($request->getSession()->get('_locale', $request->getDefaultLocale()));
        }
    }

    /**
     * @param Request $request
     */
    private function setRouterContext(Request $request)
    {
        if (null !== $this->router) {
            $this->router->getContext()->setParameter('_locale', $request->getSession()->get('_locale', $request->getDefaultLocale()));
        }
    }
}
