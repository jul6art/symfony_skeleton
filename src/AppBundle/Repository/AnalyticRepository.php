<?php

namespace AppBundle\Repository;

use AdminBundle\AbstractClasses\AbstractRepository;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;

/**
 * AnalyticRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AnalyticRepository extends AbstractRepository
{
    /**
     * @param QueryBuilder $builder
     * @param \DateTime    $date
     *
     * @return QueryBuilder
     */
    public function filterByDateAfter(QueryBuilder $builder, \DateTime $date)
    {
        $builder
            ->andWhere($builder->expr()->gt('entity.date', ':date'))
            ->setParameter('date', $date, Type::DATETIME);

        return $builder;
    }

    /**
     * @param \DateTime $date
     * @param string    $ip
     *
     * @return array
     *
     * @throws ORMException
     */
    public function findAfterDate(\DateTime $date, $ip = '')
    {
        $builder = $this->createQueryBuilder('entity');

        $this
            ->filterById($builder, $ip);

        $this
            ->filterByDateAfter($builder, $date);

        return $builder->getQuery()->getResult();
    }
}
