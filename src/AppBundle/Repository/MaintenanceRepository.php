<?php

namespace AppBundle\Repository;

use AdminBundle\AbstractClasses\AbstractRepository;
use AppBundle\Entity\Maintenance;
use Doctrine\ORM\NonUniqueResultException;

/**
 * MaintenanceRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MaintenanceRepository extends AbstractRepository
{
    /**
     * @param int $id
     *
     * @return Maintenance|null
     *
     * @throws NonUniqueResultException
     */
    public function findById($id = 0)
    {
        $builder = $this->createQueryBuilder('entity');

        $this
            ->filterById($builder, $id);

        $builder
            ->setMaxResults(1);

        return $builder->getQuery()->getOneOrNullResult();
    }

    /**
     * @return Maintenance|null
     *
     * @throws NonUniqueResultException
     */
    public function findFirst()
    {
        $builder = $this->createQueryBuilder('entity');

        $builder
            ->setMaxResults(1);

        return $builder->getQuery()->getOneOrNullResult();
    }
}
