<?php

namespace AppBundle\Controller;

use AppBundle\Manager\MaintenanceManager;
use Doctrine\ORM\NonUniqueResultException;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController.
 */
class DefaultController extends FOSRestController
{
    /**
     * @Route("/", name="homepage", defaults={"_locale"="%locale%"}, requirements={"_locale" = "%available_locale%"})
     * @Route("/{_locale}/", name="app_home", requirements={"_locale" = "%available_locale%"})
     *
     * @return Response
     */
    public function indexAction()
    {
        $view = $this->view()
                     ->setTemplate('@App/Default/index.html.twig');

        return $this->handleView($view);
    }

    /**
     * @Route("/{_locale}/maintenance", name="app_maintenance")
     *
     * @return Response
     *
     * @throws NonUniqueResultException
     */
    public function maintenanceAction()
    {
        $view = $this->view()
                     ->setTemplate('GkratzMaintenanceBundle:base:maintenance.html.twig')
                     ->setTemplateData([
                         'maintenance' => $this->get(MaintenanceManager::class)->find(),
                     ]);

        return $this->handleView($view);
    }
}
