<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 21/08/2018
 * Time: 04:29.
 */

namespace AppBundle\Controller;

use Doctrine\ORM\OptimisticLockException;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use UserBundle\Entity\User;

/**
 * @Route("/{_locale}")
 *
 * Class StuffController.
 */
class StuffController extends FOSRestController
{
    /**
     * @param Request $request
     * @param User    $user
     *
     * @Route("/garage/{id}", name="app_garage", requirements={"id": "\d+"})
     *
     * @return RedirectResponse
     *
     * @throws OptimisticLockException
     */
    public function garageAction(Request $request, User $user)
    {
        //		$this->denyAccessUnlessGranted(UserVoter::RESTORE, $user);
//
        //		$this->get('user_manager')->restore($user);

        return $this->redirect($this->generateUrl('admin_user_list'));
    }
}
