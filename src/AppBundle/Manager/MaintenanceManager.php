<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 23/08/2018
 * Time: 15:28.
 */

namespace AppBundle\Manager;

use AppBundle\Entity\Maintenance;
use AppBundle\Repository\MaintenanceRepository;
use AdminBundle\Service\IpService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class MaintenanceManager.
 */
class MaintenanceManager
{
    /**
     * @var MaintenanceRepository
     */
    private $maintenanceRepository;

    /**
     * @var IpService
     */
    private $ipService;

    /**
     * MaintenanceManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param IpService              $ipService
     */
    public function __construct(EntityManagerInterface $entityManager, IpService $ipService)
    {
        $this->maintenanceRepository = $entityManager->getRepository(Maintenance::class);
        $this->ipService = $ipService;
    }

    /**
     * @return Maintenance|null
     *
     * @throws NonUniqueResultException
     */
    public function find()
    {
        return $this->maintenanceRepository->findFirst();
    }

    /**
     * @param Maintenance $maintenance
     * @param bool        $status
     *
     * @throws OptimisticLockException
     */
    public function updateStatus(Maintenance $maintenance, $status = true)
    {
        $maintenance->setMaintenance($status);

        $this->save($maintenance);
    }

    /**
     * @param Maintenance $maintenance
     *
     * @throws OptimisticLockException
     */
    public function updateIp(Maintenance $maintenance)
    {
        $ip = $this->ipService->getIp();

        $maintenance->addExceptionIp($ip);

        $this->save($maintenance);
    }

    /**
     * @param Maintenance $maintenance
     *
     * @throws OptimisticLockException
     */
    public function removeIp(Maintenance $maintenance)
    {
        $ip = $this->ipService->getIp();

        $maintenance->removeExceptionIp($ip);

        $this->save($maintenance);
    }

    /**
     * @param Maintenance $maintenance
     * @param string      $date
     * @param string      $time
     *
     * @throws OptimisticLockException
     */
    public function updateDate(Maintenance $maintenance, $date = '', $time = '')
    {
        try {
            $datetime = new \DateTime($date.' '.$time);
            $maintenance->setDate($datetime);

            $this->save($maintenance);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * @param Maintenance $maintenance
     *
     * @throws OptimisticLockException
     */
    public function save(Maintenance $maintenance)
    {
        $this->maintenanceRepository->save($maintenance);
    }
}
