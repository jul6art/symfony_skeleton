<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 23/08/2018
 * Time: 15:28.
 */

namespace AppBundle\Manager;

use AdminBundle\Event\AnalyticEvent;
use AppBundle\Entity\Analytic;
use AppBundle\Repository\AnalyticRepository;
use AdminBundle\Service\BrowserService;
use AdminBundle\Service\IpService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Gkratz\AnalyticBundle\Utils\AnalyticCharts;
use Gkratz\AnalyticBundle\Utils\GeolocalisationIp;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;

/**
 * Class AnalyticManager.
 */
class AnalyticManager
{
    /**
     * @var AnalyticRepository
     */
    private $analyticRepository;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var GeolocalisationIp
     */
    private $geolocalisationIp;

    /**
     * @var IpService
     */
    private $ipService;

    /**
     * @var BrowserService
     */
    private $browserService;

    /**
     * @var TraceableEventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var AnalyticCharts
     */
    private $analyticCharts;

    /**
     * AnalyticManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        $locale = '',
        GeolocalisationIp $geolocalisationIp,
        IpService $ipService,
        BrowserService $browserService,
        TraceableEventDispatcher $eventDispatcher,
        AnalyticCharts $analyticCharts
    ) {
        $this->analyticRepository = $entityManager->getRepository(Analytic::class);
        $this->locale = $locale;
        $this->geolocalisationIp = $geolocalisationIp;
        $this->ipService = $ipService;
        $this->browserService = $browserService;
        $this->eventDispatcher = $eventDispatcher;
        $this->analyticCharts = $analyticCharts;
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        try {
            $ip = $this->ipService->getIp();
            $referer = $request->server->get('HTTP_REFERER');

            $page = $request->getPathInfo();
            if ('' !== $_SERVER['QUERY_STRING']) {
                $page .= sprintf('?%s', $_SERVER['QUERY_STRING']);
            }

            $analytic = (new Analytic())
                ->setIp($ip)
                ->setCountry($this->geolocalisationIp->lookup($ip, $this->locale))
                ->setCity($this->geolocalisationIp->lookupCity($ip, $this->locale))
                ->setHost(gethostbyaddr($ip))
                ->setBrowser($this->browserService->getBrowser())
                ->setReferer(!is_null($referer) ? $referer : '')
                ->setPage($page)
                ->setNewSession(0 === count($this->findAfterDate($ip)) ? true : false)
                ->setLanguage($request->getLocale());

            $this->save($analytic);

            $this->eventDispatcher->dispatch(AnalyticEvent::ADDED, new AnalyticEvent($analytic));
        } catch (\Exception $e) {
            // continue, it's not so important
        }
    }

    /**
     * @return array
     */
    public function findAllGraphes()
    {
        return [
            // visits of the week
            'sevenDaysPerUser' => $this->analyticCharts->sevenDaysPerUser(),
            'sevenDaysPerCountry' => $this->analyticCharts->sevenDaysPerCountry(),
            'pieOneWeekPerCountry' => $this->analyticCharts->pieOneWeekPerCountry(),
            'sevenDaysPerCity' => $this->analyticCharts->sevenDaysPerCity(),
            'pieOneWeekPerCity' => $this->analyticCharts->pieOneWeekPerCity(),
            'sevenDaysPerLanguage' => $this->analyticCharts->sevenDaysPerLanguage(),
            'pieOneWeekPerLanguage' => $this->analyticCharts->pieOneWeekPerLanguage(),
            'sevenDaysPerBrowser' => $this->analyticCharts->sevenDaysPerBrowser(),
            'pieOneWeekPerBrowser' => $this->analyticCharts->pieOneWeekPerBrowser(),
            // visits of the year
            'oneYearPerUser' => $this->analyticCharts->oneYearPerUser(),
            'oneYearPerCountry' => $this->analyticCharts->oneYearPerCountry(),
            'pieOneYearPerCountry' => $this->analyticCharts->pieOneYearPerCountry(),
            'oneYearPerCity' => $this->analyticCharts->oneYearPerCity(),
            'pieOneYearPerCity' => $this->analyticCharts->pieOneYearPerCity(),
            'oneYearPerLanguage' => $this->analyticCharts->oneYearPerLanguage(),
            'pieOneYearPerLanguage' => $this->analyticCharts->pieOneYearPerLanguage(),
            'oneYearPerBrowser' => $this->analyticCharts->oneYearPerBrowser(),
            'pieOneYearPerBrowser' => $this->analyticCharts->pieOneYearPerBrowser(),
            // views of the week
            'sevenDaysPages' => $this->analyticCharts->sevenDaysPages(),
            'sevenDaysPerPage' => $this->analyticCharts->sevenDaysPerPage(),
            'pieOneWeekPerPage' => $this->analyticCharts->pieOneWeekPerPage(),
            // views of the year
            'oneYearPages' => $this->analyticCharts->oneYearPages(),
            'oneYearPerPage' => $this->analyticCharts->oneYearPerPage(),
            'pieOneYearPerPage' => $this->analyticCharts->pieOneYearPerPage(),
        ];
    }

    /**
     * @param string $ip
     *
     * @return Analytic[]
     *
     * @throws \Exception
     */
    public function findAfterDate($ip = '')
    {
        $date = new \Datetime();
        $date->sub(new \DateInterval('PT10M'));

        return $this->analyticRepository->findAfterDate($date, $ip);
    }

    /**
     * @param Analytic $analytic
     *
     * @throws OptimisticLockException
     */
    public function save(Analytic $analytic)
    {
        $this->analyticRepository->save($analytic);
    }
}
