<?php

namespace AdminBundle\Controller;

use AdminBundle\Constants\Constants;
use AdminBundle\Event\UserEvent;
use AdminBundle\Form\Filter\UserFilterType;
use AdminBundle\Service\FormService;
use AdminBundle\Service\NotificationService;
use AdminBundle\Voter\UserVoter;
use Doctrine\ORM\OptimisticLockException;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use UserBundle\Entity\User;
use UserBundle\Manager\UserManager;

/**
 * @Route("/user")
 *
 * Class DefaultController
 */
class UserController extends FOSRestController
{
    /**
     * @param Request $request
     * @param int     $page
     *
     * @Route("/{page}", name="admin_user_list")
     *
     * @return Response
     *
     * @throws OptimisticLockException
     */
    public function listAction(Request $request, $page = 1)
    {
        $this->denyAccessUnlessGranted(UserVoter::VIEW, User::class);

        $page = $request->get('page', 1);

        $query = $this->get(UserManager::class)->getUserRepository()->createQuery();

        $form = $this->createForm(UserFilterType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && !$form->isValid()) {
            $request->getSession()->getFlashbag()->add('error', 'admin.notification.form.user.invalid_filters');
        } elseif ($form->isSubmitted()) {
            $this->get('session')->set($form->getName(), $request->get($form->getName()));
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $query);
            $page = 1;
        } else {
            $form = $this->createForm(UserFilterType::class);
            if ($this->get(FormService::class)->keepFilters($request, $form)) {
                $form->submit($request->getSession()->get($form->getName()));
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($form, $query);
            }
        }

        $entities = $this->get('knp_paginator')->paginate($query, $page, Constants::ITEMS_PER_PAGE, [
            'defaultSortFieldName' => $request->get('sort', 'entity.id'),
            'defaultSortDirection' => $request->get('direction', 'asc'),
            'wrap-queries' => true,
        ]);

        $this->get(NotificationService::class)->reset($this->getUser(), 'members');

        $view = $this->view()
                     ->setTemplate('@Admin/user/list.html.twig')
                     ->setTemplateData([
                         'pagination' => $entities,
                         'form' => $form->createView(),
                     ]);

        return $this->handleView($view);
    }

    /**
     * @param User $user
     *
     * @Route("/impersonate/{id}", name="admin_user_impersonate", requirements={"id": "\d+"})
     *
     * @return Response
     */
    public function impersonateAction(User $user)
    {
        $this->denyAccessUnlessGranted(UserVoter::IMPERSONATE, $user);

        return $this->redirect($this->generateUrl('app_home').'?_switch_user='.$user->getEmail());
    }

    /**
     * @param User $user
     *
     * @Route("/delete/{id}", name="admin_user_delete", requirements={"id": "\d+"})
     *
     * @return RedirectResponse
     *
     * @throws OptimisticLockException
     */
    public function deleteAction(User $user)
    {
        $this->denyAccessUnlessGranted(UserVoter::DELETE, $user);

        $this->get(UserManager::class)->delete($user);

        $this->get('event_dispatcher')->dispatch(UserEvent::DELETED, new UserEvent($user));

        return $this->redirect($this->generateUrl('admin_user_list'));
    }

    /**
     * @param User $user
     *
     * @Route("/restore/{id}", name="admin_user_restore", requirements={"id": "\d+"})
     *
     * @return RedirectResponse
     *
     * @throws OptimisticLockException
     */
    public function restoreAction(User $user)
    {
        $this->denyAccessUnlessGranted(UserVoter::RESTORE, $user);

        $this->get(UserManager::class)->restore($user);

        $this->get('event_dispatcher')->dispatch(UserEvent::RESTORED, new UserEvent($user));

        return $this->redirect($this->generateUrl('admin_user_list'));
    }

    /**
     * @param User $user
     *
     * @Route("/status/{id}/{status}", name="admin_user_status", requirements={"id": "\d+", "status": "\d+"})
     *
     * @return RedirectResponse
     *
     * @throws OptimisticLockException
     */
    public function statusAction(User $user, $status = 0)
    {
        $this->denyAccessUnlessGranted(UserVoter::CHANGE_STATUS, $user);

        $this->get(UserManager::class)->changeStatus($user, $status);

        $this->get('event_dispatcher')->dispatch(UserEvent::STATUS_UPDATED, new UserEvent($user));

        return $this->redirect($this->generateUrl('admin_user_list'));
    }
}
