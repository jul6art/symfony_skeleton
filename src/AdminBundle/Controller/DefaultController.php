<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 27/08/2018
 * Time: 07:16.
 */

namespace AdminBundle\Controller;

use AdminBundle\Service\IpService;
use AdminBundle\Service\WeatherService;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController.
 */
class DefaultController extends FOSRestController
{
    /**
     * @param Request $request
     *
     * @Route("/weather", name="admin_weather")
     *
     * @return Response
     */
    public function weatherAction(Request $request)
    {
        $ip = $this->get(IpService::class)->getIp();

        $position = $this->get('gkratz_analytic_geoloc_ip')->lookupCity($ip, 'en');

        if ('Unknown city' == $position) {
            $position = 'paris';
        }

        $session = $request->getSession();

        if ($session->has('weather')) {
            $weather = json_decode($session->get('weather'));
        } else {
            $weather = $this->get(WeatherService::class)->getWeather($position);

            if (!$weather) {
                return new Response('');
            }

            $session->set('weather', json_encode($weather));
            $session->save();
        }

        $view = $this->view()
                     ->setTemplate('@Admin/includes/weather.html.twig')
                     ->setTemplateData([
                         'temperature' => $weather->current->temp_c,
                         'position' => $weather->location->name,
                         'condition' => $this->get(WeatherService::class)->getCondition($weather->current->condition->text),
                     ]);

        return $this->handleView($view);
    }
}
