<?php

/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 24-11-16
 * Time: 11:51.
 */

namespace AdminBundle\Controller;

use AdminBundle\Constants\Constants;
use AdminBundle\Service\TranslationService;
use AdminBundle\Voter\TranslateVoter;
use AdminBundle\Service\CacheService;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/translations")
 *
 * Class ProfilerController
 */
class ProfilerController extends FOSRestController
{
    /**
     * @Route("/update", name="admin_translation_update")
     */
    public function updateAction(Request $request)
    {
        $this->denyAccessUnlessGranted(TranslateVoter::EDIT, 'translate');

        $locale = $request->getSession()->get('_locale');
        $code = Constants::STATUS_HTTP_OK;
        $translationService = $this->get(TranslationService::class);
        $message = $translationService->getErrorMessage(Constants::STATUS_HTTP_OK, $locale);

        if (!$this->isCsrfTokenValid('gkratzadminbundleprofilertranslation', $request->request->get('_csrf_token'))) {
            //ERROR 606 /invalid csrf token
            $code = Constants::TRANSLATE_STATUS_INVALID_CSRF_TOKEN;
            $message = $translationService->getErrorMessage($code, $locale);
        }

        if (Constants::STATUS_HTTP_OK === $code) {
            $datas = $translationService->getFormData($request->request->get('data'));

            if (0 == count($datas)) {
                //ERROR 601 /error empty form post
                $code = Constants::TRANSLATE_STATUS_EMPTY_PARAM_ERROR;
                $message = $translationService->getErrorMessage($code, $locale);
            } else {
                $catalogue = $this->get('translator')->getCatalogue($locale);
                $resultSetTranslations = $translationService->set($datas, $locale, $catalogue);
                if (is_int($resultSetTranslations)) {
                    $code = $resultSetTranslations;
                    $message = $translationService->getErrorMessage($code, $locale);
                }
            }
        }

        if (Constants::STATUS_HTTP_OK === $code) {
            $resultCacheClear = $this->get(CacheService::class)->clear('/translations');
            if (!$resultCacheClear) {
                //ERROR 603 /error clearing the cache
                $code = Constants::TRANSLATE_STATUS_CACHE_CLEAR_ERROR;
                $message = $translationService->getErrorMessage($code, $locale);
            }
        }

        return new JsonResponse([
            'httpcode' => $code,
            'message' => $message,
        ]);
    }
}
