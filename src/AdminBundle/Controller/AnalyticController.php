<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 24/08/2018
 * Time: 02:21.
 */

namespace AdminBundle\Controller;

use AdminBundle\Voter\AnalyticVoter;
use AppBundle\Entity\Analytic;
use AppBundle\Manager\AnalyticManager;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/analytics")
 *
 * Class AnalyticController
 */
class AnalyticController extends FOSRestController
{
    /**
     * @Route("/", name="admin_analytics")
     *
     * @return Response
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted(AnalyticVoter::VIEW, Analytic::class);

        $view = $this->view()
                     ->setTemplate('@Admin/analytic/index.html.twig')
                     ->setTemplateData(
                         $this->get(AnalyticManager::class)->findAllGraphes()
                     );

        return $this->handleView($view);
    }
}
