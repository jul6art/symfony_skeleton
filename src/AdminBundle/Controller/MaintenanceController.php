<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 23/08/2018
 * Time: 15:09.
 */

namespace AdminBundle\Controller;

use AdminBundle\Event\MaintenanceEvent;
use AdminBundle\Service\CacheService;
use AdminBundle\Voter\MaintenanceVoter;
use AdminBundle\Form\MaintenanceType;
use AppBundle\Manager\MaintenanceManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/maintenance")
 *
 * Class MaintenanceController
 */
class MaintenanceController extends FOSRestController
{
    /**
     * @param Request $request
     *
     * @Route("/", name="admin_maintenance")
     *
     * @return RedirectResponse|Response
     *
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    public function indexAction(Request $request)
    {
        $maintenance = $this->get(MaintenanceManager::class)->find();

        $this->denyAccessUnlessGranted(MaintenanceVoter::EDIT, $maintenance);

        $form = $this->createForm(MaintenanceType::class, $maintenance);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get(MaintenanceManager::class)->updateDate($maintenance, $form['date']->getData(), $form['time']->getData());

            $this->get('event_dispatcher')->dispatch(MaintenanceEvent::EDITED, new MaintenanceEvent($maintenance));

            return $this->redirectToRoute('admin_maintenance');
        }

        $view = $this->view()
                     ->setTemplate('@Admin/maintenance/index.html.twig')
                     ->setTemplateData([
                         'maintenance' => $maintenance,
                         'form' => $form->createView(),
                     ]);

        return $this->handleView($view);
    }

    /**
     * @param int $state
     *
     * @Route("/switch/{state}", name="admin_maintenance_switch")
     *
     * @return RedirectResponse
     *
     * @throws OptimisticLockException
     * @throws NonUniqueResultException
     */
    public function swicthAction($state = 0)
    {
        $maintenanceManager = $this->get(MaintenanceManager::class);

        $maintenance = $maintenanceManager->find();

        $this->denyAccessUnlessGranted(MaintenanceVoter::CHANGE_STATUS, $maintenance);

        $maintenanceManager->updateStatus($maintenance, $state);

        $this->get('event_dispatcher')->dispatch(MaintenanceEvent::STATUS_UPDATED, new MaintenanceEvent($maintenance));

        return $this->redirectToRoute('admin_maintenance');
    }

    /**
     * @Route("/ip", name="admin_maintenance_ip")
     *
     * @return RedirectResponse
     *
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    public function ipAction()
    {
        $maintenanceManager = $this->get(MaintenanceManager::class);

        $maintenance = $maintenanceManager->find();

        $this->denyAccessUnlessGranted(MaintenanceVoter::EDIT, $maintenance);

        $maintenanceManager->updateIp($maintenance);

        $this->get('event_dispatcher')->dispatch(MaintenanceEvent::IP_UPDATED, new MaintenanceEvent($maintenance));

        return $this->redirectToRoute('admin_maintenance');
    }

    /**
     * @Route("/ip/remove", name="admin_maintenance_ip_remove")
     *
     * @return RedirectResponse
     *
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    public function ipRemoveAction()
    {
        $maintenanceManager = $this->get(MaintenanceManager::class);

        $maintenance = $maintenanceManager->find();

        $this->denyAccessUnlessGranted(MaintenanceVoter::EDIT, $maintenance);

        $maintenanceManager->removeIp($maintenance);

        $this->get('event_dispatcher')->dispatch(MaintenanceEvent::IP_REMOVED, new MaintenanceEvent($maintenance));

        return $this->redirectToRoute('admin_maintenance');
    }

    /**
     * @Route("/cache", name="admin_maintenance_cache")
     *
     * @return RedirectResponse
     *
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    public function cacheAction()
    {
        $maintenanceManager = $this->get(MaintenanceManager::class);

        $maintenance = $maintenanceManager->find();

        $this->denyAccessUnlessGranted(MaintenanceVoter::CLEAR_CACHE, $maintenance);

        $this->get(CacheService::class)->clear();

        $this->get('event_dispatcher')->dispatch(MaintenanceEvent::CACHE_CLEARED, new MaintenanceEvent($maintenance));

        return $this->redirectToRoute('admin_maintenance');
    }
}
