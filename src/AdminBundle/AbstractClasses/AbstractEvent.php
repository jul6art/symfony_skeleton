<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 22/08/2018
 * Time: 09:43.
 */

namespace AdminBundle\AbstractClasses;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class AbstractEvent.
 */
abstract class AbstractEvent extends Event
{
    /**
     * @var ArrayCollection
     */
    protected $data;

    /**
     * AbstractEvent constructor.
     */
    public function __construct()
    {
        $this->data = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $data
     *
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}
