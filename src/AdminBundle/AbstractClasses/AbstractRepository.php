<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 21/08/2018
 * Time: 01:57.
 */

namespace AdminBundle\AbstractClasses;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use AdminBundle\Constants\Constants;

/**
 * Class AbstractRepository.
 */
abstract class AbstractRepository extends EntityRepository
{
    const NO_FLUSH = 0;
    const FLUSH = 1;

    /**
     * @param QueryBuilder $builder
     * @param int          $id
     *
     * @return QueryBuilder
     */
    public function filterById(QueryBuilder $builder, $id = 0)
    {
        $builder
            ->andWhere($builder->expr()->eq('entity.id', ':id'))
            ->setParameter('id', $id, Type::INTEGER);

        return $builder;
    }

    /**
     * @param QueryBuilder $builder
     * @param string       $sort
     * @param string       $direction
     *
     * @return QueryBuilder
     */
    protected function sort(QueryBuilder $builder, $sort = '', $direction = '')
    {
        $builder
            ->orderBy($sort, $direction);

        return $builder;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $builder = $this->createQueryBuilder('entity');

        return $builder->getQuery()->getResult();
    }

    /**
     * @return int
     */
    public function countAll()
    {
        return count($this->findAll());
    }

    /**
     * @param int    $page
     * @param string $sort
     * @param string $direction
     *
     * @return Paginator
     */
    public function findAllPaginated($page = 0, $sort = '', $direction = '')
    {
        $builder = $this->createQueryBuilder('entity');

        $this
            ->sort($builder, $sort, $direction);

        $this
            ->paginate($builder, $page);

        return new Paginator($builder);
    }

    /**
     * @param QueryBuilder $builder
     * @param $page
     *
     * @return QueryBuilder
     */
    protected function paginate(QueryBuilder $builder, $page)
    {
        $firstResult = ($page * Constants::ITEMS_PER_PAGE) - Constants::ITEMS_PER_PAGE;

        $builder
            ->setFirstResult($firstResult)
            ->setMaxResults(Constants::ITEMS_PER_PAGE);

        return $builder;
    }

    /**
     * Save an Entity entity.
     *
     * @param mixed $entity
     * @param int   $flag
     *
     * @throws OptimisticLockException
     */
    public function save($entity, $flag = self::FLUSH)
    {
        $this->getEntityManager()->persist($entity);

        if ($flag & self::FLUSH) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Delete an Entity.
     *
     * @param mixed $entity
     * @param int   $flag
     *
     * @throws OptimisticLockException
     */
    public function delete($entity, $flag = self::FLUSH)
    {
        $this->getEntityManager()->remove($entity);

        if ($flag & self::FLUSH) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Flushes all changes.
     *
     * @throws OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * Clear entity manager.
     */
    public function clear()
    {
        $this->getEntityManager()->clear();
    }
}
