<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 26/08/2018
 * Time: 22:46.
 */

namespace AdminBundle\Transformer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use UserBundle\Entity\User;

/**
 * Class UserTransformer.
 */
class UserTransformer implements NormalizerInterface
{
    /**
     * @param mixed $user
     * @param null  $format
     * @param array $context
     *
     * @return array|bool|float|int|string
     */
    public function normalize($user, $format = null, array $context = array())
    {
        if (!$user instanceof User) {
            return [];
        }

        return [
            'id' => $user->getId(),
        ];
    }

    /**
     * @param mixed $data
     * @param null  $format
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof User;
    }
}
