<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 22/08/2018
 * Time: 09:46.
 */

namespace AdminBundle\Event;

use AdminBundle\AbstractClasses\AbstractEvent;
use UserBundle\Entity\User;

/**
 * Class UserEvent.
 */
class UserEvent extends AbstractEvent
{
    const ADDED = 'user.event.added';
    const EDITED = 'user.event.edited';
    const DELETED = 'user.event.deleted';
    const RESTORED = 'user.event.restored';
    const STATUS_UPDATED = 'user.event.status_updated';

    /**
     * @var User
     */
    private $user;

    /**
     * UserEvent constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
