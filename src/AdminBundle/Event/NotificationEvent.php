<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 22/08/2018
 * Time: 09:46.
 */

namespace AdminBundle\Event;

use AdminBundle\AbstractClasses\AbstractEvent;
use UserBundle\Entity\User;

/**
 * Class NotificationEvent.
 */
class NotificationEvent extends AbstractEvent
{
    const INCREMENT_ABUSES = 'notification.event.increment_abuses';
    const INCREMENT_MEMBERS = 'notification.event.increment_members';
    const INCREMENT_MESSAGES = 'notification.event.increment_messages';
    const INCREMENT_FOLLOWERS = 'notification.event.increment_followers';

    /**
     * @var User
     */
    private $user;

    /**
     * NotificationEvent constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}
