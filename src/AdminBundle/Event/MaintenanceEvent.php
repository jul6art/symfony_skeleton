<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 22/08/2018
 * Time: 09:46.
 */

namespace AdminBundle\Event;

use AdminBundle\AbstractClasses\AbstractEvent;
use AppBundle\Entity\Maintenance;

/**
 * Class MaintenanceEvent.
 */
class MaintenanceEvent extends AbstractEvent
{
    const EDITED = 'maintenance.event.edited';
    const IP_UPDATED = 'maintenance.event.ip_updated';
    const IP_REMOVED = 'maintenance.event.ip_removed';
    const CACHE_CLEARED = 'maintenance.event.cache_cleared';
    const STATUS_UPDATED = 'maintenance.event.status_updated';
    /**
     * @var Maintenance
     */
    private $maintenance;

    /**
     * MaintenanceEvent constructor.
     *
     * @param Maintenance $maintenance
     */
    public function __construct(Maintenance $maintenance)
    {
        parent::__construct();
        $this->maintenance = $maintenance;
    }

    /**
     * @return Maintenance
     */
    public function getMaintenance()
    {
        return $this->maintenance;
    }

    /**
     * @param Maintenance $maintenance
     *
     * @return $this
     */
    public function setMaintenance(Maintenance $maintenance)
    {
        $this->maintenance = $maintenance;

        return $this;
    }
}
