<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 22/08/2018
 * Time: 09:46.
 */

namespace AdminBundle\Event;

use AdminBundle\AbstractClasses\AbstractEvent;
use AppBundle\Entity\Analytic;

/**
 * Class AnalyticEvent.
 */
class AnalyticEvent extends AbstractEvent
{
    const ADDED = 'analytic.event.added';
    /**
     * @var Analytic
     */
    private $analytic;

    /**
     * AnalyticEvent constructor.
     *
     * @param Analytic $analytic
     */
    public function __construct(Analytic $analytic)
    {
        parent::__construct();
        $this->analytic = $analytic;
    }

    /**
     * @return Analytic
     */
    public function getAnalytic()
    {
        return $this->analytic;
    }

    /**
     * @param Analytic $analytic
     *
     * @return $this
     */
    public function setAnalytic(Analytic $analytic)
    {
        $this->analytic = $analytic;

        return $this;
    }
}
