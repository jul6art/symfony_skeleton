<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 22/08/2018
 * Time: 09:49.
 */

namespace AdminBundle\EventListener;

use AdminBundle\AbstractClasses\AbstractEventListener;
use AdminBundle\Event\UserEvent;
use AdminBundle\Service\NotificationService;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use UserBundle\Manager\UserManager;

class UserEventListener extends AbstractEventListener
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * UserEventListener constructor.
     *
     * @param SessionInterface    $session
     * @param UserManager         $userManager
     * @param NotificationService $notificationService
     */
    public function __construct(
        SessionInterface $session,
        UserManager $userManager,
        NotificationService $notificationService
    ) {
        parent::__construct($session);
        $this->userManager = $userManager;
        $this->notificationService = $notificationService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            UserEvent::ADDED => 'onUserAdded',
            UserEvent::EDITED => 'onUserEdited',
            UserEvent::DELETED => 'onUserDeleted',
            UserEvent::RESTORED => 'onUserRestored',
            UserEvent::STATUS_UPDATED => 'onStatusUpdated',
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
        ];
    }

    /**
     * @param UserEvent $event
     */
    public function onUserAdded(UserEvent $event)
    {
        $this->getSession()->getFlashBag()->add('success', 'admin.notification.user.added');
    }

    /**
     * @param UserEvent $event
     */
    public function onUserEdited(UserEvent $event)
    {
        $this->getSession()->getFlashBag()->add('success', 'admin.notification.user.edited');
    }

    /**
     * @param UserEvent $event
     */
    public function onUserDeleted(UserEvent $event)
    {
        $this->getSession()->getFlashBag()->add('success', 'admin.notification.user.deleted');
    }

    /**
     * @param UserEvent $event
     */
    public function onUserRestored(UserEvent $event)
    {
        $this->getSession()->getFlashBag()->add('success', 'admin.notification.user.restored');
    }

    /**
     * @param UserEvent $event
     */
    public function onStatusUpdated(UserEvent $event)
    {
        $this->getSession()->getFlashBag()->add('success', 'admin.notification.user.status_updated');
    }

    /**
     * @param FilterUserResponseEvent $event
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        $user = $event->getUser();

        if (!$user->hasRole('ROLE_ADMIN')) {
            foreach ($this->userManager->findAllAdmins() as $admin) {
                $this->notificationService->increment($admin, 'members');
            }
        }
    }
}
