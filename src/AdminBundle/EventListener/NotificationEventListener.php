<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 22/08/2018
 * Time: 09:49.
 */

namespace AdminBundle\EventListener;

use AdminBundle\AbstractClasses\AbstractEventListener;
use AdminBundle\Event\NotificationEvent;
use AdminBundle\Service\MailService;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class NotificationEventListener.
 */
class NotificationEventListener extends AbstractEventListener
{
    /**
     * @var MailService
     */
    private $mailService;

    /**
     * NotificationEventListener constructor.
     *
     * @param SessionInterface $session
     * @param MailService      $mailService
     */
    public function __construct(SessionInterface $session, MailService $mailService)
    {
        parent::__construct($session);
        $this->mailService = $mailService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            NotificationEvent::INCREMENT_ABUSES => 'onAbusesIncremented',
            NotificationEvent::INCREMENT_MEMBERS => 'onMembersIncremented',
            NotificationEvent::INCREMENT_MESSAGES => 'onMessagesIncremented',
            NotificationEvent::INCREMENT_FOLLOWERS => 'onFollowersIncremented',
        ];
    }

    /**
     * @param NotificationEvent $event
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function onAbusesIncremented(NotificationEvent $event)
    {
        $user = $event->getUser();

        $this->mailService->send('@Admin/email/new_abuse.txt.twig', $user->getEmail(), [
            'user' => $user,
        ]);
    }

    /**
     * @param NotificationEvent $event
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function onMembersIncremented(NotificationEvent $event)
    {
        $user = $event->getUser();

        $this->mailService->send('@Admin/email/new_member.txt.twig', $user->getEmail(), [
            'user' => $user,
        ]);
    }

    /**
     * @param NotificationEvent $event
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function onMessagesIncremented(NotificationEvent $event)
    {
        $user = $event->getUser();

        $this->mailService->send('@Admin/email/new_message.txt.twig', $user->getEmail(), [
            'user' => $user,
        ]);
    }

    /**
     * @param NotificationEvent $event
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function onFollowersIncremented(NotificationEvent $event)
    {
        $user = $event->getUser();

        $this->mailService->send('@Admin/email/new_follower.txt.twig', $user->getEmail(), [
            'user' => $user,
        ]);
    }
}
