<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 22/08/2018
 * Time: 09:49.
 */

namespace AdminBundle\EventListener;

use AdminBundle\AbstractClasses\AbstractEventListener;
use AdminBundle\Event\MaintenanceEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class MaintenanceEventListener.
 */
class MaintenanceEventListener extends AbstractEventListener
{
    /**
     * MaintenanceEventListener constructor.
     *
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        parent::__construct($session);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            MaintenanceEvent::IP_UPDATED => 'onIpUpdated',
            MaintenanceEvent::IP_REMOVED => 'onIpRemoved',
            MaintenanceEvent::EDITED => 'onEdited',
            MaintenanceEvent::CACHE_CLEARED => 'onCacheCleared',
            MaintenanceEvent::STATUS_UPDATED => 'onStatusUpdated',
        ];
    }

    /**
     * @param MaintenanceEvent $event
     */
    public function onIpUpdated(MaintenanceEvent $event)
    {
        $this->getSession()->getFlashBag()->add('success', 'admin.notification.maintenance.ip_updated');
    }

    /**
     * @param MaintenanceEvent $event
     */
    public function onIpRemoved(MaintenanceEvent $event)
    {
        $this->getSession()->getFlashBag()->add('success', 'admin.notification.maintenance.ip_removed');
    }

    /**
     * @param MaintenanceEvent $event
     */
    public function onEdited(MaintenanceEvent $event)
    {
        $this->getSession()->getFlashBag()->add('success', 'admin.notification.maintenance.edited');
    }

    /**
     * @param MaintenanceEvent $event
     */
    public function onCacheCleared(MaintenanceEvent $event)
    {
        $this->getSession()->getFlashBag()->add('success', 'admin.notification.maintenance.cache_cleared');
    }

    /**
     * @param MaintenanceEvent $event
     */
    public function onStatusUpdated(MaintenanceEvent $event)
    {
        $status = $event->getMaintenance()->getMaintenance();

        if ($status) {
            $this->getSession()->getFlashBag()->add('success', 'admin.notification.maintenance.status_on');
        } else {
            $this->getSession()->getFlashBag()->add('success', 'admin.notification.maintenance.status_off');
        }
    }
}
