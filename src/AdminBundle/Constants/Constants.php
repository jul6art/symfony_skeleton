<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 21/08/2018
 * Time: 02:28.
 */

namespace AdminBundle\Constants;

/**
 * Class Constants.
 */
class Constants
{
    const ITEMS_PER_PAGE = 25;
    const STATUS_HTTP_OK = 200;

    // IN FLIGHT TRANSLATION ERROR CODES
    const TRANSLATE_STATUS_FILE_WRITE_ERROR = 600;
    const TRANSLATE_STATUS_EMPTY_PARAM_ERROR = 601;
    const TRANSLATE_STATUS_MERGE_YML_ERROR = 602;
    const TRANSLATE_STATUS_CACHE_CLEAR_ERROR = 603;
    const TRANSLATE_STATUS_INVALID_CSRF_TOKEN = 606;
}
