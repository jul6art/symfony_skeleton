<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 20/08/2018
 * Time: 21:27.
 */

namespace AdminBundle\Doctrine\ORM;

use AppBundle\Entity\Maintenance;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadMaintenanceData.
 */
class LoadMaintenanceData extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // Site maintenance
        $maintenance = (new Maintenance())
            ->setMaintenance(false)
            ->addExceptionIp('127.0.0.1');

        $manager->persist($maintenance);

        $manager->flush();
    }
}
