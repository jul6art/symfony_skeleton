<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 20/08/2018
 * Time: 21:27.
 */

namespace AdminBundle\Doctrine\ORM;

use Craue\ConfigBundle\Entity\Setting;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadSettingsData.
 */
class LoadSettingsData extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // Site nickname
        $setting = $this->createSetting('site_nickname', 'cmystuff', 'site');
        $this->addReference('site_nickname', $setting);
        $manager->persist($setting);

        // Site fullname
        $setting = $this->createSetting('site_fullname', 'see my stuff', 'site');
        $this->addReference('site_fullname', $setting);
        $manager->persist($setting);

        // Site url
        $setting = $this->createSetting('site_url', 'https://cmystuff.com', 'site');
        $this->addReference('site_url', $setting);
        $manager->persist($setting);

        // Email
        $setting = $this->createSetting('email', 'admin@vsweb.be', 'contact');
        $this->addReference('email', $setting);
        $manager->persist($setting);

        // Phone
        $setting = $this->createSetting('phone', '0 (032) 497 18 99 98', 'contact');
        $this->addReference('phone', $setting);
        $manager->persist($setting);

        // Facebook ID
        $setting = $this->createSetting('facebook_id', 'https://www.facebook.com/geoffrey.stoner.9', 'network');
        $this->addReference('facebook_id', $setting);
        $manager->persist($setting);

        // Twitter ID
        $setting = $this->createSetting('twitter_id', 'https://twitter.com/deepweb_be', 'network');
        $this->addReference('twitter_id', $setting);
        $manager->persist($setting);

        $manager->flush();
    }

    /**
     * @param string $parameter
     * @param string $value
     *
     * @return Setting
     */
    private function createSetting($parameter = '', $value = '', $section = '')
    {
        $setting = new Setting();
        $setting->setName($parameter);
        $setting->setValue($value);
        $setting->setSection($section);

        return $setting;
    }
}
