<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 21/08/2018
 * Time: 01:27.
 */

namespace AdminBundle\Voter;

use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class UserVoter.
 */
class UserVoter extends Voter
{
    const ADD = 'admin.user.add';
    const EDIT = 'admin.user.edit';
    const VIEW = 'admin.user.list';
    const LIST_API = 'admin.user.list_api';
    const DELETE = 'admin.user.delete';
    const RESTORE = 'admin.user.restore';
    const CHANGE_STATUS = 'admin.user.change_status';
    const IMPERSONATE = 'admin.user.impersonate';

    /**
     * @var AccessDecisionManagerInterface
     */
    private $decisionManager;

    /**
     * UserVoter constructor.
     *
     * @param AccessDecisionManagerInterface $decisionManager
     */
    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::ADD,
            self::EDIT,
            self::VIEW,
            self::LIST_API,
            self::DELETE,
            self::RESTORE,
            self::CHANGE_STATUS,
            self::IMPERSONATE,
        ])) {
            return false;
        }

        if ($subject instanceof User) {
            return true;
        }

        if (User::class === $subject) {
            return true;
        }

        return false;
    }

    /**
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::ADD:
                return $this->canAdd($subject, $token);
            case self::EDIT:
                return $this->canEdit($subject, $token);
            case self::VIEW:
                return $this->canList($subject, $token);
            case self::LIST_API:
                return $this->canListApi($subject, $token);
            case self::DELETE:
                return $this->canDelete($subject, $token);
            case self::RESTORE:
                return $this->canRestore($subject, $token);
            case self::CHANGE_STATUS:
                return $this->canChangeStatus($subject, $token);
            case self::IMPERSONATE:
                return $this->canImpersonate($subject, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canAdd($user, TokenInterface $token)
    {
        return $this->canList($user, $token);
    }

    /**
     * @param User           $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canEdit(User $user, TokenInterface $token)
    {
        if ($user === $token->getUser()) {
            return true;
        }

        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }

    /**
     * @param $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canList($user, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }

    /**
     * @param $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canListApi($user, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }

    /**
     * @param User           $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canDelete(User $user, TokenInterface $token)
    {
        if ($user === $token->getUser()) {
            return false;
        }

        if ($user->isSoftDelete()) {
            return false;
        }

        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }

    /**
     * @param User           $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canRestore(User $user, TokenInterface $token)
    {
        if ($user === $token->getUser()) {
            return false;
        }

        if (!$user->isSoftDelete()) {
            return false;
        }

        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }

    /**
     * @param User           $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canChangeStatus(User $user, TokenInterface $token)
    {
        if ($user === $token->getUser()) {
            return false;
        }

        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }

    /**
     * @param User           $user
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canImpersonate(User $user, TokenInterface $token)
    {
        if ($user === $token->getUser()) {
            return false;
        }

        if ($this->decisionManager->decide($token, ['ROLE_ALLOWED_TO_SWITCH'])) {
            return true;
        }

        return false;
    }
}
