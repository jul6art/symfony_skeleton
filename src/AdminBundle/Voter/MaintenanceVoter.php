<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 21/08/2018
 * Time: 01:27.
 */

namespace AdminBundle\Voter;

use AppBundle\Entity\Maintenance;
use AdminBundle\Service\IpService;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class MaintenanceVoter.
 */
class MaintenanceVoter extends Voter
{
    const EDIT = 'admin.maintenance.edit';
    const VIEW = 'admin.maintenance.list';
    const ADD_IP = 'admin.maintenance.add_ip';
    const REMOVE_IP = 'admin.maintenance.remove_ip';
    const CLEAR_CACHE = 'admin.maintenance.clear_cache';
    const CHANGE_STATUS = 'admin.maintenance.change_status';

    /**
     * @var AccessDecisionManagerInterface
     */
    private $decisionManager;

    /**
     * @var IpService
     */
    private $ipService;

    /**
     * UserVoter constructor.
     *
     * @param AccessDecisionManagerInterface $decisionManager
     */
    public function __construct(AccessDecisionManagerInterface $decisionManager, IpService $ipService)
    {
        $this->decisionManager = $decisionManager;
        $this->ipService = $ipService;
    }

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::EDIT,
            self::VIEW,
            self::ADD_IP,
            self::REMOVE_IP,
            self::CLEAR_CACHE,
            self::CHANGE_STATUS,
        ])) {
            return false;
        }

        if ($subject instanceof Maintenance) {
            return true;
        }

        if (Maintenance::class === $subject) {
            return true;
        }

        return false;
    }

    /**
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($subject, $token);
            case self::VIEW:
                return $this->canList($subject, $token);
            case self::ADD_IP:
                return $this->canAddIp($subject, $token);
            case self::REMOVE_IP:
                return $this->canRemoveIp($subject, $token);
            case self::CLEAR_CACHE:
                return $this->canClearCache($subject, $token);
            case self::CHANGE_STATUS:
                return $this->canChangeStatus($subject, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param Maintenance    $maintenance
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canEdit(Maintenance $maintenance, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }

    /**
     * @param Maintenance    $maintenance
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canAddIp(Maintenance $maintenance, TokenInterface $token)
    {
        if ($maintenance->containExceptionIp($this->ipService->getIp())) {
            return false;
        }

        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }

    /**
     * @param Maintenance    $maintenance
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canRemoveIp(Maintenance $maintenance, TokenInterface $token)
    {
        if (!$maintenance->containExceptionIp($this->ipService->getIp())) {
            return false;
        }

        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }

    /**
     * @param $maintenance
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canList($maintenance, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }

    /**
     * @param Maintenance    $maintenance
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canClearCache(Maintenance $maintenance, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }

    /**
     * @param Maintenance    $maintenance
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canChangeStatus(Maintenance $maintenance, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }
}
