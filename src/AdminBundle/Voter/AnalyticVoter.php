<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 21/08/2018
 * Time: 01:27.
 */

namespace AdminBundle\Voter;

use AppBundle\Entity\Analytic;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class AnalyticVoter.
 */
class AnalyticVoter extends Voter
{
    const VIEW = 'admin.analytic.list';

    /**
     * @var AccessDecisionManagerInterface
     */
    private $decisionManager;

    /**
     * UserVoter constructor.
     *
     * @param AccessDecisionManagerInterface $decisionManager
     */
    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
            self::VIEW,
        ])) {
            return false;
        }

        if ($subject instanceof Analytic) {
            return true;
        }

        if (Analytic::class === $subject) {
            return true;
        }

        return false;
    }

    /**
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canList($subject, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param $analytic
     * @param TokenInterface $token
     *
     * @return bool
     */
    private function canList($analytic, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        return false;
    }
}
