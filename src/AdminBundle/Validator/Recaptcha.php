<?php

namespace AdminBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD"})
 */
class Recaptcha extends Constraint
{
    public $message = 'Invalid recaptcha.';
}
