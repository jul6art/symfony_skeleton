<?php

namespace AdminBundle\Validator;

use ReCaptcha\ReCaptcha as BaseReCaptcha;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class RecaptchaValidator.
 */
class RecaptchaValidator extends ConstraintValidator
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var string|null
     */
    protected $recaptchaSecret;

    /**
     * RecaptchaValidator constructor.
     *
     * @param RequestStack $requestStack
     * @param string|null  $recaptchaSecret
     */
    public function __construct(RequestStack $requestStack, $recaptchaSecret = null)
    {
        $this->requestStack = $requestStack;
        $this->recaptchaSecret = $recaptchaSecret;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof \AdminBundle\Validator\Recaptcha) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Recaptcha');
        }

        if (null === $this->recaptchaSecret) {
            return;
        }

        $gRecaptchaResponse = $this->requestStack->getMasterRequest()->get('g-recaptcha-response');
        $clientIp = $this->requestStack->getMasterRequest()->getClientIp();

        $recaptcha = new BaseReCaptcha($this->recaptchaSecret);

        $resp = $recaptcha->verify($gRecaptchaResponse, $clientIp);

        if (!$resp->isSuccess()) {
            $this->context->buildViolation($constraint->message)
                ->atPath('recaptcha')
                ->addViolation();
        }
    }
}
