<?php

namespace AdminBundle\Service;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\CacheClearer\CacheClearerInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Class CacheService.
 */
class CacheService
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var CacheClearerInterface
     */
    private $cacheClearer;

    /**
     * @var Kernel
     */
    private $kernel;

    /**
     * @var string
     */
    private $kernelCacheDir;

    public function __construct(Filesystem $filesystem, CacheClearerInterface $cacheClearer, Kernel $kernel, $kernelCacheDir = '')
    {
        $this->filesystem = $filesystem;
        $this->cacheClearer = $cacheClearer;
        $this->kernel = $kernel;
        $this->kernelCacheDir = $kernelCacheDir;
    }

    /**
     * @param string $subDossier
     *
     * @return bool
     */
    public function clear($subDossier = '')
    {
        try {
            if ('' !== $subDossier) {
                $realCacheDir = $this->kernelCacheDir.$subDossier;
                $this->cacheClearer->clear($realCacheDir);
                $this->filesystem->remove($realCacheDir);
            } else {
                $application = new Application($this->kernel);
                $application->setAutoExit(false);

                $input = new ArrayInput([
                    'command' => 'cache:clear',
                    '--env' => $this->kernel->getEnvironment(),
                ]);

                $output = new BufferedOutput();
                $application->run($input, $output);

                $output = $output->fetch()."\n";
            }

            $result = true;
        } catch (\Exception $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * @param $dir
     *
     * @return int
     */
    public function getSize($dir)
    {
        $size = 0;
        foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each) {
            $size += is_file($each) ? filesize($each) : $this->getSize($each);
        }

        return $size;
    }
}
