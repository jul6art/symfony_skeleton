<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 27/08/2018
 * Time: 04:15.
 */

namespace AdminBundle\Service;

use Swift_Mailer;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Templating\TemplateReferenceInterface;

/**
 * Class MailService.
 */
class MailService
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var string
     */
    private $senderName;

    /**
     * @var string
     */
    private $address;

    /**
     * MailService constructor.
     *
     * @param Swift_Mailer    $mailer
     * @param EngineInterface $templating
     * @param string          $senderName
     * @param string          $address
     */
    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, $senderName = '', $address = '')
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->senderName = $senderName;
        $this->address = $address;
    }

    /**
     * @param string|TemplateReferenceInterface $template
     * @param string                            $to
     * @param array                             $parameters
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function send($template = '', $to = '', array $parameters)
    {
        $content = $this->templating->render($template, $parameters);

        $renderedLines = explode("\n", trim($content));
        $subject = array_shift($renderedLines);
        $body = implode("\n", $renderedLines);

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setTo($to)
            ->setFrom([
                $this->address => $this->senderName,
            ])
            ->setBody($body, 'text/html');

        $this->mailer->send($message);
    }
}
