<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 27/08/2018
 * Time: 07:22.
 */

namespace AdminBundle\Service;

/**
 * Class WeatherService.
 */
class WeatherService
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $apikey;

    /**
     * WeatherService constructor.
     *
     * @param string $apikey
     */
    public function __construct($url = '', $apikey = '')
    {
        $this->url = $url;
        $this->apikey = $apikey;
    }

    /**
     * @param string $position
     *
     * @return bool|mixed
     */
    public function getWeather($position = '')
    {
        $url = sprintf('%s?key=%s&q=%s',
            $this->url,
            $this->apikey,
            $position
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $json_output = curl_exec($ch);

        if (200 !== curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
            return false;
        }

        $weather = json_decode($json_output);

        curl_close($ch);

        return $weather;
    }

    /**
     * @param string $condition
     *
     * @return string
     */
    public function getCondition($condition = '')
    {
        switch ($condition) {
            //SNOW
            case 'Moderate or heavy snow with thunder':
                $condition = 'snow';
                break;
            case 'Patchy light snow with thunder':
                $condition = 'snow';
                break;
            case 'Moderate or heavy showers of ice pellets':
                $condition = 'snow';
                break;
            case 'Light showers of ice pellets':
                $condition = 'snow';
                break;
            case 'Light snow showers':
                $condition = 'snow';
                break;
            case 'Ice pellets':
                $condition = 'snow';
                break;
            case 'Heavy snow':
                $condition = 'snow';
                break;
            case 'Light snow':
                $condition = 'snow';
                break;
            case 'Patchy moderate snow':
                $condition = 'snow';
                break;
            case 'Moderate snow':
                $condition = 'snow';
                break;
            case 'Patchy heavy snow':
                $condition = 'snow';
                break;
            case 'Moderate or heavy snow showers':
                $condition = 'snow';
                break;
            case 'Patchy light snow':
                $condition = 'snow';
                break;
            case 'Blowing snow':
                $condition = 'snow';
                break;

            //RAINY
            case 'Light sleet showers':
                $condition = 'rainy';
                break;
            case 'Light rain shower':
                $condition = 'rainy';
                break;
            case 'Moderate or heavy sleet':
                $condition = 'rainy';
                break;
            case 'Light sleet':
                $condition = 'rainy';
                break;
            case 'Moderate or heavy freezing rain':
                $condition = 'rainy';
                break;
            case 'Light freezing rain':
                $condition = 'rainy';
                break;
            case 'Light rain':
                $condition = 'rainy';
                break;
            case 'Patchy light rain':
                $condition = 'rainy';
                break;
            case 'Freezing drizzle':
                $condition = 'rainy';
                break;
            case 'Light drizzle':
                $condition = 'rainy';
                break;
            case 'Patchy light drizzle':
                $condition = 'rainy';
                break;

            //STORMY RAINY
            case 'Moderate or heavy rain shower':
                $condition = 'stormyRainy';
                break;
            case 'Torrential rain shower':
                $condition = 'stormyRainy';
                break;
            case 'Moderate or heavy sleet showers':
                $condition = 'stormyRainy';
                break;
            case 'Patchy light rain with thunder':
                $condition = 'stormyRainy';
                break;
            case 'Heavy rain':
                $condition = 'stormyRainy';
                break;
            case 'Heavy rain at times':
                $condition = 'stormyRainy';
                break;
            case 'Moderate rain':
                $condition = 'stormyRainy';
                break;
            case 'Moderate rain at times':
                $condition = 'stormyRainy';
                break;
            case 'Heavy freezing drizzle':
                $condition = 'stormyRainy';
                break;

            //TORNADO STORMY RAINY
            case 'Moderate or heavy rain with thunder':
                $condition = 'tornadoStormyRainy';
                break;

            //TORNADO THUNDER
            case 'Thundery outbreaks possible':
                $condition = 'tornadoThunder';
                break;

            //CLOUDY
            case 'Overcast':
                $condition = 'cloudy';
                break;
            case 'Partly cloudy':
                $condition = 'cloudy';
                break;
            case 'Cloudy':
                $condition = 'cloudy';
                break;

            //SUN
            case 'Sunny':
                $condition = 'sun';
                break;
            case 'Clear':
                $condition = 'sun';
                break;

            //CLOUD
            case 'Mist':
                $condition = 'cloud';
                break;
            case 'Patchy rain possible':
                $condition = 'cloud';
                break;
            case 'Patchy snow possible':
                $condition = 'cloud';
                break;
            case 'Patchy sleet possible':
                $condition = 'cloud';
                break;
            case 'Patchy freezing drizzle possible':
                $condition = 'cloud';
                break;
            case 'Blizzard':
                $condition = 'cloud';
                break;
            case 'Freezing fog':
                $condition = 'cloud';
                break;
            case 'Fog':
                $condition = 'cloud';
                break;
            default:
                $condition = 'cloud';
                break;
        }

        return $condition;
    }
}
