<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 26/08/2018
 * Time: 22:21.
 */

namespace AdminBundle\Service;

use AdminBundle\Event\NotificationEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use UserBundle\Entity\User;
use UserBundle\Manager\UserManager;

/**
 * Class NotificationService.
 */
class NotificationService
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * NotificationService constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     * @param UserManager              $userManager
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, UserManager $userManager)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->userManager = $userManager;
    }

    /**
     * @param User   $user
     * @param string $property
     * @param int    $value
     */
    public function set(User $user, $property = '', $value = 0)
    {
        $setter = sprintf('set%sCount', ucfirst($property));

        $user->$setter($value);
    }

    /**
     * @param User   $user
     * @param string $property
     *
     * @return mixed
     */
    public function get(User $user, $property = '')
    {
        $getter = sprintf('get%sCount', ucfirst($property));

        return $user->$getter();
    }

    /**
     * @param User   $user
     * @param string $property
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function reset(User $user, $property = '')
    {
        $this->set($user, $property, 0);

        $this->userManager->save($user);
    }

    /**
     * @param User   $user
     * @param string $property
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function increment(User $user, $property = '')
    {
        $value = $this->get($user, $property) + 1;
        $this->set($user, $property, $value);

        $constSuffix = strtoupper($property);

        $this->eventDispatcher->dispatch(constant("AdminBundle\Event\NotificationEvent::INCREMENT_$constSuffix"), new NotificationEvent($user));

        $this->userManager->save($user);
    }
}
