<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 24/08/2018
 * Time: 23:33.
 */

namespace AdminBundle\Service;

use AdminBundle\Constants\Constants;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Translation\MessageCatalogue;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class TranslationService.
 */
class TranslationService
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var Finder
     */
    private $finder;

    /**
     * @var MessageCatalogue
     */
    private $catalogue;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator, $kernelRootDir = '', $translationsPath = '')
    {
        $this->translator = $translator;
        $this->path = $kernelRootDir.$translationsPath;
        $this->finder = new Finder();
    }

    /**
     * @param array $datas
     *
     * @return array
     */
    public function getFormData(array $datas)
    {
        $result = [];
        array_walk($datas, function ($value, $key) use (&$result) {
            if ('' != $value) {
                $result[$key] = $value;
            }
        });

        return $result;
    }

    /**
     * @param array            $datas
     * @param string           $locale
     * @param MessageCatalogue $catalogue
     *
     * @return bool|int
     */
    public function set(array $datas, $locale = '', MessageCatalogue $catalogue)
    {
        $this->locale = $locale;
        $this->filename = 'messages.'.$this->locale;
        $this->catalogue = $catalogue;

        $result = true;

        if (is_null($this->catalogue->getFallbackCatalogue())) {
            $messages = $this->catalogue->all();
        } else {
            $messages = array_replace_recursive($this->catalogue->getFallbackCatalogue()->all(), $this->catalogue->all());
        }

        foreach ($datas as $key => $translation) {
            foreach ($messages as $domain => $arrayTranslations) {
                if (key_exists($key, $arrayTranslations)) {
                    $filename = sprintf('%s.%s.yml', $domain, $this->locale);
                    $resultWrite = $this->write($filename, $key, $translation);

                    if (is_int($resultWrite)) {
                        return $resultWrite;
                    }

                    unset($datas[$key]);
                }
            }
        }

        if (count($datas) > 0) {
            foreach ($datas as $key => $translation) {
                $resultWrite = $this->write($this->filename.'.yml', $key, $translation);

                if (is_int($resultWrite)) {
                    return $resultWrite;
                }

                unset($datas[$key]);
            }
        }

        return $result;
    }

    /**
     * @param string $filename
     * @param string $key
     * @param string $translation
     *
     * @return bool|int
     */
    private function write($filename = '', $key = '', $translation = '')
    {
        $result = true;

        $fileContent = $this->getFileContent($filename);

        $resultMergeYml = $this->mergeYml($fileContent, $key, $translation);

        if (is_int($resultMergeYml)) {
            //ERROR 602 /error managing the xml dom
            $result = $resultMergeYml;
        } else {
            $resultSaveFile = $this->saveFile($resultMergeYml, $filename);

            if (!$resultSaveFile) {
                //ERROR 600 /error writing file
                $result = Constants::TRANSLATE_STATUS_FILE_WRITE_ERROR;
            }
        }

        return $result;
    }

    /**
     * @param string $filename
     *
     * @return bool|string
     */
    private function getFileContent($filename = '')
    {
        $result = false;

        $this->finder->files()->in($this->path);

        foreach ($this->finder as $file) {
            if ($file->getRelativePathname() == $filename) {
                $result = $file->getContents();
            }
        }

        return $result;
    }

    /**
     * @param string $fileContent
     * @param string $key
     * @param string $value
     *
     * @return int|mixed
     */
    private function mergeYml($fileContent = '', $key = '', $value = '')
    {
        try {
            $result = Yaml::parse($fileContent);
            $result[$key] = $value;
        } catch (\Exception $e) {
            $result = Constants::TRANSLATE_STATUS_MERGE_YML_ERROR;
        }

        return $result;
    }

    /**
     * @param array  $arrays
     * @param string $filename
     *
     * @return bool
     */
    private function saveFile(array $array, $filename = '')
    {
        try {
            $yaml = Yaml::dump($array);
            file_put_contents($this->path.$filename, $yaml);
            $result = true;
        } catch (\Exception $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * @param int    $error
     * @param string $locale
     *
     * @return string
     */
    public function getErrorMessage($error = 0, $locale = '')
    {
        return $this->translator->trans('error.ajax_translation.'.$error, [], 'error', $locale);
    }
}
