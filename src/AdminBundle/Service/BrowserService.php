<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 23/08/2018
 * Time: 22:55.
 */

namespace AdminBundle\Service;

/**
 * Class BrowserService.
 */
class BrowserService
{
    /**
     * @return string
     */
    public function getBrowser()
    {
        try {
            $browserString = $_SERVER['HTTP_USER_AGENT'];

            $preSplit = explode('0)', $browserString);
            if (!empty($preSplit[1])) {
                $morceaux = explode('/', $preSplit[1]);
                if (' Gecko' == $morceaux[0]) {
                    $browser = 'moz';
                } else {
                    $browser = 'div';
                }
            } else {
                $browser = 'chr';
            }
        } catch (\Exception $e) {
            $browser = 'div';
        }

        return $browser;
    }
}
