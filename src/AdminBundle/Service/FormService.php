<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 29/08/2018
 * Time: 10:46.
 */

namespace AdminBundle\Service;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FormService.
 */
class FormService
{
    /**
     * @param Request       $request
     * @param FormInterface $form
     *
     * @return bool
     */
    public function keepFilters(Request $request, FormInterface $form)
    {
        $keepForm = (null !== $request->getSession()->get($form->getName()));

        if (!$keepForm) {
            $request->getSession()->remove($form->getName());
        }

        return $keepForm;
    }
}
