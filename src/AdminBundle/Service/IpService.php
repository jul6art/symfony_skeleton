<?php

namespace AdminBundle\Service;

/**
 * Class IpService.
 */
class IpService
{
    /**
     * @return mixed
     */
    public function getIp()
    {
        try {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            return $ip;
        } catch (\Exception $e) {
            return 'NaN';
        }
    }
}
