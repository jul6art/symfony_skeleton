<?php

namespace AdminBundle\Twig\Extension;

use AdminBundle\Service\NotificationService;
use Twig\TwigFunction;
use UserBundle\Entity\User;

/**
 * Class NotificationExtension.
 */
class NotificationExtension extends \Twig_Extension
{
    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * NotificationExtension constructor.
     *
     * @param NotificationService $notificationService
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * @return array|\Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('notification', [$this, 'notification'], ['is_safe' => ['html']]),
            new TwigFunction('notificationPage', [$this, 'notificationPage']),
        ];
    }

    /**
     * @param $user
     * @param string $property
     *
     * @return string
     */
    public function notification($user, $property = '')
    {
        if ($user instanceof User) {
            $count = $this->notificationService->get($user, $property);

            if ($count > 0) {
                return sprintf(
                    '<span class="badge badge-danger" style="font-size: 12px!important">%s</span>',
                    $count
                );
            }
        }

        return '';
    }

    /**
     * @param $user
     *
     * @return string
     */
    public function notificationPage($user)
    {
        if ($user instanceof User) {
            $count = $this->notificationService->get($user, 'totalNotifications');

            if ($count > 0) {
                return sprintf('(%s) ', $count);
            }
        }

        return '';
    }
}
