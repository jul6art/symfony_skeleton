<?php

namespace AdminBundle\Twig\Extension;

use Twig\TwigFilter;

/**
 * Class ActiveExtension.
 */
class ActiveExtension extends \Twig_Extension
{
    /**
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return array(
            new TwigFilter('active', [$this, 'activeClass']),
        );
    }

    /**
     * @param $current
     * @param $routes
     *
     * @return string
     */
    public function activeClass($current, $routes)
    {
        if (in_array($current, $routes)) {
            return 'active';
        }

        return '';
    }
}
