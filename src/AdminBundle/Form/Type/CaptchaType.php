<?php
/**
 * Created by PhpStorm.
 * User: VanIllaSkyPE
 * Date: 25/03/2018
 * Time: 23:38.
 */

namespace AdminBundle\Form\Type;

use AdminBundle\Validator\Recaptcha;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CaptchaType.
 */
class CaptchaType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'required' => false,
            'mapped' => false,
            'constraints' => [
                new Recaptcha(),
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'captcha';
    }

    /**
     * @return null|string
     */
    public function getParent()
    {
        return TextType::class;
    }
}
