<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 25/08/2018
 * Time: 18:45.
 */

namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TimePickerType.
 */
class TimePickerType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'class' => 'time',
            ],
        ]);
    }

    /**
     * @return null|string
     */
    public function getParent()
    {
        return TextType::class;
    }
}
