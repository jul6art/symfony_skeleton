<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 30/08/2018
 * Time: 17:10.
 */

namespace AdminBundle\Form\Filter\Type;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RoleFilterType.
 */
class RoleFilterType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'required' => false,
            'placeholder' => 'form.select.all',
            'choices' => [
                'form.select.all' => '',
                'admin.form.filters.user.roles.role_user' => 'ROLE_USER',
                'admin.form.filters.user.roles.role_admin' => 'ROLE_ADMIN',
            ],
            'attr' => [
                'form' => 'form_filter',
                'class' => 'select2-search',
            ],
        ]);
    }

    /**
     * @return null|string
     */
    public function getParent()
    {
        return ChoiceFilterType::class;
    }
}
