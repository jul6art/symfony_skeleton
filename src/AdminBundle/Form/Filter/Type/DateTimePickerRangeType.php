<?php

namespace AdminBundle\Form\Filter\Type;

use AdminBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DateTimePickerRangeType.
 */
class DateTimePickerRangeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('left_datetime', DateTimePickerType::class, $options['left_options']);

        $builder->add('right_datetime', DateTimePickerType::class, $options['right_options']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'required' => false,
            'left_options' => [],
            'right_options' => [],
        ]);
    }
}
