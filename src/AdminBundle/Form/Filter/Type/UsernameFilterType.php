<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 30/08/2018
 * Time: 17:10.
 */

namespace AdminBundle\Form\Filter\Type;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use UserBundle\Manager\UserManager;

/**
 * Class UsernameFilterType.
 */
class UsernameFilterType extends AbstractType
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * UserFilterType constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RouterInterface $router, UserManager $userManager)
    {
        $this->router = $router;
        $this->userManager = $userManager;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws ORMException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $users = $this->userManager->findForForm();
        array_unshift($users, ['form.select.placeholder' => '']);

        $resolver->setDefaults([
            'required' => false,
            'placeholder' => 'form.select.all',
            'choices' => $users,
            'attr' => [
                'form' => 'form_filter',
                'class' => 'select2-ajax',
                'data-url' => $this->router->generate('api_user_list'),
            ],
        ]);
    }

    /**
     * @return null|string
     */
    public function getParent()
    {
        return ChoiceFilterType::class;
    }
}
