<?php
/**
 * Created by PhpStorm.
 * User: gkratz
 * Date: 28/08/2018
 * Time: 06:13.
 */

namespace AdminBundle\Form\Filter;

use AdminBundle\Form\Filter\Type\RoleFilterType;
use AdminBundle\Form\Filter\Type\StatusFilterType;
use AdminBundle\Form\Filter\Type\UsernameFilterType;
use AdminBundle\Form\Filter\Type\DateTimePickerRangeType;
use Doctrine\ORM\ORMException;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Manager\UserManager;

/**
 * Class UserFilterType.
 */
class UserFilterType extends AbstractType
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * UserFilterType constructor.
     *
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @throws ORMException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', UsernameFilterType::class, [
            'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }

                return $this->userManager->getUserRepository()->filterById($filterQuery->getQueryBuilder(), (int) $values['value']);
            },
        ]);

        $builder->add('lastLogin', DateTimePickerRangeType::class, [
            'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                if (empty($values['value']['left_datetime']) && empty($values['value']['right_datetime'])) {
                    return null;
                }

                return $this->userManager->getUserRepository()->filterByDaterange($filterQuery->getQueryBuilder(), $values['value']);
            },
            'left_options' => [
                'required' => false,
                'label' => 'admin.form.filters.user.left_datetime',
                'attr' => [
                    'form' => 'form_filter',
                    'class' => 'datetime input-sm',
                ],
            ],
            'right_options' => [
                'required' => false,
                'label' => 'admin.form.filters.user.right_datetime',
                'attr' => [
                    'form' => 'form_filter',
                    'class' => 'datetime input-sm',
                ],
            ],
        ]);

        $builder->add('roles', RoleFilterType::class, [
            'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }

                return $this->userManager->getUserRepository()->filterByRole($filterQuery->getQueryBuilder(), $values['value']);
            },
        ]);

        $builder->add('status', StatusFilterType::class, [
            'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }

                return $this->userManager->getUserRepository()->filterByStatus($filterQuery->getQueryBuilder(), $values['value']);
            },
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'translation_domain' => 'form',
        ]);
    }
}
