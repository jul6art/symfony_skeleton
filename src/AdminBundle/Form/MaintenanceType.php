<?php

namespace AdminBundle\Form;

use AdminBundle\Form\Type\DatePickerType;
use AdminBundle\Form\Type\TimePickerType;
use AppBundle\Entity\Maintenance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaintenanceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('date', DatePickerType::class, [
            'data' => is_null($options['data']->getDate()) ? '' : $options['data']->getDate()->format('Y-m-d'),
            'mapped' => false,
            'label' => 'form.maintenance.label.date',
        ]);

        $builder->add('time', TimePickerType::class, [
            'data' => is_null($options['data']->getDate()) ? '' : $options['data']->getDate()->format('H:i'),
            'mapped' => false,
            'label' => 'form.maintenance.label.time',
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Maintenance::class,
            'translation_domain' => 'form',
        ]);
    }
}
