<?php

namespace ApiBundle\Controller;

use AdminBundle\Voter\UserVoter;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use UserBundle\Entity\User;
use UserBundle\Manager\UserManager;

/**
 * Class UserController.
 *
 * @Route("/user")
 */
class UserController extends FOSRestController
{
    /**
     * @param Request $request
     *
     * @Route("/", name="api_user_list")
     *
     * @return JsonResponse
     *
     * @throws ORMException
     */
    public function listAction(Request $request)
    {
        $this->denyAccessUnlessGranted(UserVoter::LIST_API, User::class);

        $query = $request->get('q', null);

        if (is_null($query)) {
            return new JsonResponse([]);
        }

        return new JsonResponse([
            'results' => $this->get(UserManager::class)->findForApi($query),
        ]);
    }
}
