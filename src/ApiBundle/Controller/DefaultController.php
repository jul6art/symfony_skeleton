<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController.
 */
class DefaultController extends FOSRestController
{
    /**
     * @Route("/search", name="api_search")
     */
    public function searchAction(Request $request)
    {
        $view = $this->view()
                     ->setTemplate('@App/Default/index.html.twig');

        return $this->handleView($view);
    }
}
